'use strict';

var chai = chai || require('chai');
var metrocore = metrocore || require('../metrocore');

var should = chai.should();

var PeerModule = metrocore.Peer;
var Peer;

describe('Peer', function() {
  it('should initialze the main object', function() {
    should.exist(PeerModule);
  });
  it('should be able to create class', function() {
    Peer = PeerModule;
    should.exist(Peer);
  });
  it('should be able to create instance', function() {
    var p = new Peer('localhost', 7555);
    should.exist(p);
  });
  it('should be able to create instance', function() {
    var p = new Peer('localhost:7555');
    should.exist(p);
  });
  it('should be able to create instance', function() {
    var p = new Peer('localhost:7555');
    var p2 = new Peer(p);
    should.exist(p2);
  });
  it('should not be able to create instance', function() {
    should.throw(function() {
      new Peer(7555);
    });
  });
  it('should be able to create instance', function() {
    var p = new Peer('localhost', 7555);
    p.toString().should.equal('localhost:7555');
  });
  it('check host as buffer', function() {
    var p = new Peer('127.0.0.1', 7555);
    p.getHostAsBuffer().toString('hex').should.equal('7f000001');
  });
});





