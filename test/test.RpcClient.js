'use strict';

var chai = chai || require('chai');
var metrocore = metrocore || require('../metrocore');

var should = chai.should();

var RpcClientModule = metrocore.RpcClient;
var RpcClient;
    RpcClient = RpcClientModule;

describe('RpcClient', function() {
  it('should initialze the main object', function() {
    should.exist(RpcClientModule);
  });
  it('should be able to create class', function() {
    should.exist(RpcClient);
  });
  it('should be able to create instance', function() {
    var s = new RpcClient();
    should.exist(s);
  });
});





