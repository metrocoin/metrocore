'use strict';

var chai = chai || require('chai');
var metrocore = metrocore || require('../metrocore');

var expect = chai.expect;
var should = chai.should();

describe('Initialization of metrocore', function() {
  it('should initialze the main object', function() {
    should.exist(metrocore);
  });
});
