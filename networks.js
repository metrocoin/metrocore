var Put = require('bufferput');
var buffertools = require('buffertools');
var hex = function(hex) {
  return new Buffer(hex, 'hex');
};

exports.livenet = {
  name: 'livenet',
  magic: hex('63697479'),
  addressVersion: 0x32,
  privKeyVersion: 128,
  P2SHVersion: 5,
  hkeyPublicVersion: 0x0488b21e,
  hkeyPrivateVersion: 0x0488ade4,
  genesisBlock: {
    hash: hex('f5ac27dd85bb8814e1527d794f86e1f1adbc398f4eec75a688bd952c00000000'),
    merkle_root: hex('f46ddbe1d622513c4cf570a2af19040d5d66dd01fe2f2c6041b255512d2ca000'),
    height: 0,
    nonce: 1038648149,
    version: 1,
    prev_hash: buffertools.fill(new Buffer(32), 0),
    timestamp: 1519627908,
    bits: 486604799,
  },
  dnsSeeds: [
    'seed.metrocoin.me',
    'dnsseed.metrocoin.me'
  ],
  defaultClientPort: 9555
};

exports.mainnet = exports.livenet;

exports.testnet = {
  name: 'testnet',
  magic: hex('66726565'),
  addressVersion: 0x6f,
  privKeyVersion: 239,
  P2SHVersion: 196,
  hkeyPublicVersion: 0x043587cf,
  hkeyPrivateVersion: 0x04358394,
  genesisBlock: {
    hash: hex('0574f3a28653c6b12d342f48fac44be703cca68d4989cca98d14b8a500000000'),
    merkle_root: hex('f46ddbe1d622513c4cf570a2af19040d5d66dd01fe2f2c6041b255512d2ca000'),
    height: 0,
    nonce: 384568319,
    version: 1,
    prev_hash: buffertools.fill(new Buffer(32), 0),
    timestamp: 1519627908,
    bits: 486604799,
  },
  dnsSeeds: [
    'testnet-seed.metrocoin.petertodd.org',
    'testnet-seed.bluematt.me'
  ],
  defaultClientPort: 19555
};
