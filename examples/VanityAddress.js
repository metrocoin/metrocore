'use strict';


var run = function() {
  // Replace '../metrocore' with 'metrocore' if you use this code elsewhere.
  var metrocore = require('../metrocore');
  var Key = metrocore.Key;
  var Address = metrocore.Address;

  // config your regular expression
  var re = /[0-9]{6}$/; // ends in 6 digits

  var a, k, m;
  while (true) {
    k = Key.generateSync();
    a = Address.fromKey(k);
    m = a.toString().match(re);
    if (m) break;
  }
  console.log('Address: ' + a.toString());
  console.log('Private Key: ' + k.private.toString('hex'));

};

module.exports.run = run;
if (require.main === module) {
  run();
}
