var metrocore = require('../metrocore');
var Address = metrocore.Address;
var metrocoreUtil = metrocore.util;
var Script = metrocore.Script;
var network = metrocore.networks.livenet;


var script = ''; // write down your script here
var s = Script.fromHumanReadable(script);
var hash = metrocoreUtil.sha256ripe160(s.getBuffer());
var version = network.addressScript;

var addr = new Address(version, hash);
var addrStr = addr.as('base58');

// This outputs the "address" of thescript
console.log(addrStr);
