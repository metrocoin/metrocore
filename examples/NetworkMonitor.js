'use strict';

var run = function() {
  // Replace '../metrocore' with 'metrocore' if you use this code elsewhere.
  var metrocore = require('../metrocore');
  var NetworkMonitor = metrocore.NetworkMonitor;

  var config = {
    networkName: 'testnet',
    host: 'localhost',
    port: 17555
  };


  var nm = new NetworkMonitor.create(config);
  // monitor incoming transactions to http://tpfaucet.appspot.com/ donation address
  nm.incoming('msj42CCGruhRsFrGATiUuh25dtxYtnpbTx', function(tx) {
    console.log('Donation to tpfaucet! '+JSON.stringify(tx.getStandardizedObject()));
  });

  // connect to metrocoin network and start listening
  nm.start();

};

module.exports.run = run;
if (require.main === module) {
  run();
}
